var CalendarChecker = require("./calendar-checker-common");
var Application     = require("application");
var Utils           = require("utils/utils");

var Plugin          = com.noizeramp.calendarchecker.CalendarCheckerPlugin;
var PluginListener  = com.noizeramp.calendarchecker.CalendarCheckerPluginListener;

CalendarChecker.addOnNotificationClickedListener = function(callback) {
  return new Promise(function(resolve, reject) {
    try {
      // note that this is ONLY triggered when the user clicked the notification in the statusbar
      Plugin.setOnMessageReceivedCallback(
          new PluginListener({
            success: function(notification) {
              callback(JSON.parse(notification));
            }
          })
      );
      resolve();
    } catch (ex) {
      console.log("Error in CalendarChecker.addOnNotificationClickedListener: " + ex);
      reject(ex);
    }
  });
};

// Start periodic calendar checker
CalendarChecker.start = function(interval, blacklist) {
  var context = Application.android.foregroundActivity;
  interval = interval || CalendarChecker.defaults.interval;
  Plugin.start(context, interval, blacklist);
};

// Stop periodic calendar checker
CalendarChecker.stop = function() {
  var context = Application.android.foregroundActivity;
  Plugin.stop(context);
};

module.exports = CalendarChecker;
