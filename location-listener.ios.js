var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};

var ListenerImpl = (function (_super) {
    __extends(ListenerImpl, _super);
    function ListenerImpl() {
        _super.apply(this, arguments);
    }

    ListenerImpl.initWithCallbacks = function(onEvent, onError) {
        var listener = ListenerImpl.new();
        listener._onEvent = onEvent;
        listener._onError = onError;
        return listener;
    };

    ListenerImpl.prototype.locationManagerDidUpdateLocations = function(manager, locations) {
      // we are interested in just the event, not the locations
      if (this._onEvent) this._onEvent();
    };

    ListenerImpl.prototype.locationManagerDidFailWithError = function(manager, error) {
      if (this._onError) {
        this._onError(new Error(error.localizedDescription));
      }
    };

    ListenerImpl.prototype.locationManagerDidChangeAuthorizationStatus = function(manager, status) {
      // status changed
      console.log("CC.locationManagerDidChangeAuthorizationStatus: " + status);
    };

    ListenerImpl.ObjCProtocols = [CLLocationManagerDelegate];
    return ListenerImpl;
}(NSObject));

module.exports = ListenerImpl;
