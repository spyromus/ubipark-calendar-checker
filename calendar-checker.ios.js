var CalendarChecker = require("./calendar-checker-common");
var Calendar        = require("nativescript-calendar");
var Application     = require("application");
var Settings        = require("application-settings");
var Listener        = require("./location-listener");
var Geolocation     = require("nativescript-geolocation");
var Utils           = require("utils/utils");

var HOUR          = 60 * 60 * 1000;

var started = false;
var timerInterval = CalendarChecker.defaults.interval;
var timer = null;
var locationManager;
var locationListener;
var blacklistRegexp = null;

// Adds the notification click listener (Android only)
CalendarChecker.addOnNotificationClickedListener = function(_callback) {
  return new Promise(function(resolve, _reject) {
    // TODO wire this
    resolve();
  });
};

// Start periodic calendar checker
CalendarChecker.start = function(interval, blacklist) {
  blacklistRegexp = new RegExp(blacklist, "g");

  return new Promise(function(resolve, _reject) {
    if (started) return resolve(false);

    started = true;

    Application.on(Application.resumeEvent, onResume);
    Application.on(Application.suspendEvent, onSuspend);

    timerInterval = interval;
    startTimer();

    resolve(true);
  });
};

// Stop periodic calendar checker
CalendarChecker.stop = function() {
  return new Promise(function(resolve, _reject) {
    if (!started) return resolve(false);

    Application.off(Application.resumeEvent, onResume);
    Application.off(Application.suspendEvent, onSuspend);

    stopTimer();
    stopSLC();

    started = false;

    resolve(true);
  });
};

function onResume() {
  console.log("[CalendarChecker] onResume");
  stopSLC();
  startTimer();
}

function onSuspend() {
  console.log("[CalendarChecker] onSuspend");
  stopTimer();
  startSLC();
}

function startTimer() {
  stopTimer();
  timer = setInterval(onCheckEvent, timerInterval);
  onCheckEvent();
}

function stopTimer() {
  if (timer) clearInterval(timer);
  timer = null;
}

function startSLC() {
  if (locationManager) return;

  locationListener = Listener.initWithCallbacks(onCheckEvent, onCheckError);
  locationManager = new CLLocationManager();
  locationManager.delegate = locationListener;
  locationManager.startMonitoringSignificantLocationChanges();
}

function stopSLC() {
  if (locationManager) {
    locationManager.stopMonitoringSignificantLocationChanges();
    locationManager.delegate = null;

    locationListener = null;
    locationManager  = null;

    return true;
  } else {
    return false;
  }
}

function checkEvent(geoCoder, event, cur, region, resolve, reject) {
  geoCoder.geocodeAddressStringInRegionCompletionHandler(event.location, region, function(placemarks, error) {
    if (!error) {
      var place = placemarks.objectAtIndex(0);
      if (place) {
        var loc = place.location;
        var meters = loc.distanceFromLocation(cur);

        console.log("[CalendarChecker] Distance from '" + event.location + "': " + meters + " m");
        if (meters > 500) {
          var latitude  = loc.coordinate.latitude;
          var longitude = loc.coordinate.longitude;

          var startTime = event.startDate ? new Date(event.startDate).getTime() : new Date().getTime();
          var endTime   = event.endDate   ? new Date(event.endDate).getTime()   : startTime + 2 * HOUR;

          presentNotification({
            title:          "UbiPark",
            body:           "Upcoming event at " + event.location,
            eventTitle:     event.title,
            eventLocation:  event.location,
            eventStartTime: startTime,
            eventEndTime:   endTime,
            latitude:       latitude,
            longitude:      longitude,
            eventId:        event.id
          });

          if (resolve) resolve();
        }
      } else {
        console.log("[CalendarChecker] Failed to geocode (no place) '" + event.location + "'");
        if (reject) reject();
      }
    } else {
      console.log("[CalendarChecker] Failed to geocode '" + event.location + "'");
      if (reject) reject();
    }
  });
}

function presentNotification(options) {
  var notification = UILocalNotification.alloc().init();
  notification.alertTitle = options.title;
  notification.alertBody = options.body;
  notification.timeZone = Utils.ios.getter(NSTimeZone, NSTimeZone.defaultTimeZone);
  notification.applicationIconBadgeNumber = options.badge || 0;
  notification.soundName = "app/sounds/honkhonk.caf";

  // these are sent back to the plugin when a notification is received
  var userInfoDict = NSMutableDictionary.alloc().initWithCapacity(3);
  userInfoDict.setObjectForKey(options.eventId, "eventId");
  userInfoDict.setObjectForKey(options.title, "title");
  userInfoDict.setObjectForKey(options.body, "body");
  userInfoDict.setObjectForKey(options.eventTitle, "eventTitle");
  userInfoDict.setObjectForKey(options.eventLocation, "eventLocation");
  userInfoDict.setObjectForKey(options.eventStartTime, "eventStartTime");
  userInfoDict.setObjectForKey(options.eventEndTime, "eventEndTime");
  userInfoDict.setObjectForKey(options.latitude, "latitude");
  userInfoDict.setObjectForKey(options.longitude, "longitude");

  notification.userInfo = userInfoDict;

  var app = Utils.ios.getter(UIApplication, UIApplication.sharedApplication);
  app.presentLocalNotificationNow(notification);
}

function onCheckEvent() {
  var startDate = getStartDate();
  var endDate   = getEndDate();

  console.log("[CalendarChecker] Checking events in range: " + startDate + " - " + endDate);

  var options = { startDate: startDate, endDate: endDate };
  Calendar.findEvents(options).then(function(events) {
    var res = [];
    var startTs = startDate.getTime();

    for (var e of events) {
      var loc = (e.location || "").trim();
      if (loc != "" && startTs < e.startDate && isNotBlacklisted(loc)) res.push(e);
    }

    if (res.length > 0) {
      console.log("[CalendarChecker] Found events");
      console.log(JSON.stringify(res));

      Geolocation.getCurrentLocation({
        desiredAccuracy: 3,
        updateDistance: 1,
        timeout: 30000
      }).then(function(currentLocation) {
        console.log("[CalendarChecker] Got location");
        var geoCoder = CLGeocoder.alloc().init();
        var cur = CLLocation.alloc().initWithLatitudeLongitude(currentLocation.latitude, currentLocation.longitude);
        var region = CLCircularRegion.alloc().initWithCenterRadiusIdentifier(cur.coordinate, 50000, "current");

        for (var event of res) checkEvent(geoCoder, event, cur, region);
      }, function(e) {
        console.log("[CalendarChecker] Failed to get location: " + e);
      });
    } else {
      console.log("[CalendarChecker] No events");
    }
  }, function(error) {
    console.log("[CalendarChecker] Error: " + error);
  });
}

function isNotBlacklisted(loc) {
  return !blacklistRegexp || !loc.match(blacklistRegexp);
}

function onCheckError(e) {
  console.log("[CalendarChecker] Error=: " + e);
}

function getStartDate() {
  var ts = Settings.getNumber("calendarChecker.startTime", -1);
  return ts !== -1 ? new Date(ts) : new Date();
}

function getEndDate() {
  var ts = new Date().getTime() + HOUR;
  Settings.setNumber("calendarChecker.startTime", ts);
  return new Date(ts);
}

module.exports = CalendarChecker;
