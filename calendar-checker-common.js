var CalendarChecker = {};

CalendarChecker.defaults = {
  interval: 15 * 60 * 1000 // 15 minutes
};

module.exports = CalendarChecker;
