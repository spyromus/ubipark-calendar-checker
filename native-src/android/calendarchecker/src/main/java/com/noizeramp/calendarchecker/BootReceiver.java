package com.noizeramp.calendarchecker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static com.noizeramp.calendarchecker.CalendarCheckerPlugin.TAG;

/**
 * Created by alg on 03/08/16.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, intent.getAction());
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.d(TAG, "Boot completed");

            // If plugin is marked as running, re-start it with last known interval
            if (CalendarCheckerPlugin.isRunning(context)) {
                Log.d(TAG, "Restarting");
                CalendarCheckerPlugin.start(context, CalendarCheckerPlugin.getInterval(context), CalendarCheckerPlugin.getBlacklist(context));
            }
        }
    }
}
