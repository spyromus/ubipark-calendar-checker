package com.noizeramp.calendarchecker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

import org.json.JSONObject;

public class CalendarCheckerPlugin {
    static final String TAG = "CalendarChecker";
    private static final long DELAY_MS = 5 * 1000;

    static boolean isActive = false;
    private static JSONObject cachedData;
    private static CalendarCheckerPluginListener onMessageReceivedCallback;

    public static boolean isRunning(Context context) {
        return getPreferences(context).getBoolean("isRunning", false);
    }

    public static int getInterval(Context context) {
        return getPreferences(context).getInt("interval", -1);
    }

    public static String getBlacklist(Context context) {
        return getPreferences(context).getString("blacklist", null);
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(TAG, 0);
    }

    private static void setRunning(Context context, boolean v) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean("isRunning", v);
        editor.commit();
    }

    /**
     * Starts the background checker.
     *
     * @param context   application context.
     * @param interval  check interval (in ms).
     * @param blacklist regexp string or NULL.
     */
    public static void start(Context context, int interval, String blacklist) {
        AlarmManager alarmManager = getAlarmManager(context);
        PendingIntent pendingIntent = getPendingIntent(context);

        // Cancel any previously scheduled intents
        alarmManager.cancel(pendingIntent);
        setRunning(context, false);

        // Schedule periodic intent with approximate period (to be battery efficient)
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, DELAY_MS, interval, pendingIntent);

        getPreferences(context).edit()
                .putInt("interval", interval)
                .putString("blacklist", blacklist)
                .commit();

        setRunning(context, true);
    }

    /**
     * Stops the background checker.
     *
     * @param context application context.
     */
    public static void stop(Context context) {
        AlarmManager alarmManager = getAlarmManager(context);
        PendingIntent pendingIntent = getPendingIntent(context);

        // Cancel any previously scheduled intents
        alarmManager.cancel(pendingIntent);

        setRunning(context, false);
    }

    private static AlarmManager getAlarmManager(Context context) {
        return (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    private static PendingIntent getPendingIntent(Context context) {
        Intent intent = new Intent(context, CalendarChecker.class);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    /**
     * Set the on message received callback
     *
     * @param callbacks for data.
     */
    public static void setOnMessageReceivedCallback(CalendarCheckerPluginListener callbacks) {
        onMessageReceivedCallback = callbacks;

        if (cachedData != null) {
            executeOnMessageReceivedCallback(cachedData);
            cachedData = null;
        }
    }

    /**
     * Execute the onMessageReceivedCallback with the data passed.
     * In case the callback is not present, cache the data;
     *
     * @param data
     */
    public static void executeOnMessageReceivedCallback(JSONObject data) {
        if (onMessageReceivedCallback != null) {
            Log.d(TAG, "Sending message to client");
            onMessageReceivedCallback.success(data);
        } else {
            Log.d(TAG, "No callback function - caching the data for later retrieval.");
            cachedData = data;
        }
    }

}
