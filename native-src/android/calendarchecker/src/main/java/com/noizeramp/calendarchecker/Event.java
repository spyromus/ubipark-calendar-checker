package com.noizeramp.calendarchecker;

/**
 * Created by alg on 03/08/16.
 */

public class Event {
    int id;
    String calendar;
    String title;
    String location;
    long startTime;
    long endTime;

    Event(int id, String calendar, String title, String location, long startTime, long endTime) {
        this.id = id;
        this.calendar = calendar;
        this.title = title;
        this.location = location;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
