package com.noizeramp.calendarchecker;

/**
 * Created by alg on 03/08/16.
 */

public interface CalendarCheckerPluginListener {
    /**
     * Defines a success callback method, which is used to pass success function reference
     * from the nativescript to the Java plugin
     *
     * @param data
     */
    void success(Object data);


    /**
     * Defines a error callback method, which is used to pass error function reference
     * from the nativescript to the Java plugin
     *
     * @param data
     */
    void error(Object data);
}