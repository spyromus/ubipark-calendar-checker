package com.noizeramp.calendarchecker;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarChecker extends BroadcastReceiver implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "CalendarChecker";
    public static final String BLACKLIST = "blacklist";
    private static final float THRESHOLD_METERS = 500;
    private static double EARTH_RADIUS = 6371.0;

    private ArrayList<Event> events;
    private Context context;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        String blacklistString = CalendarCheckerPlugin.getBlacklist(context);

        Calendar startDate = getStartDate(context);
        Calendar endDate = getEndDate(context);

        DateFormat format = SimpleDateFormat.getDateTimeInstance();
        Log.d(TAG, "Checking for events in range " + format.format(startDate.getTime()) + " - " + format.format(endDate.getTime()) + " with blacklist " + blacklistString);

        Uri uri = CalendarContract.Events.CONTENT_URI;
        String[] projection = {CalendarContract.Events._ID, CalendarContract.Events.CALENDAR_DISPLAY_NAME, CalendarContract.Events.TITLE, CalendarContract.Events.EVENT_LOCATION, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND};
        String selection = "((" + CalendarContract.Events.DTSTART + " > ?) AND (" + CalendarContract.Events.DTSTART + " <= ?))";
        String[] selectionArgs = {Long.toString(startDate.getTimeInMillis()), Long.toString(endDate.getTimeInMillis())};

        ContentResolver cr = context.getContentResolver();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "No calendar access permission");
        } else {
            // Query for the events in range
            Cursor cursor = cr.query(uri, projection, selection, selectionArgs, null);
            events = new ArrayList<>();

            if (cursor != null) {
                try {
                    while (cursor.moveToNext()) {
                        int id = cursor.getInt(0);
                        String calendar = cursor.getString(1);
                        String title = cursor.getString(2);
                        String location = cursor.getString(3);
                        long startTime = cursor.getLong(4);
                        long endTime = cursor.getLong(5);

                        // Ignore events with no locations
                        if (location != null && !location.trim().equals("") && (blacklistString == null || !location.matches(blacklistString))) {
                            events.add(new Event(id, calendar, title, location, startTime, endTime));
                        }
                    }
                } finally {
                    cursor.close();
                }
            }

            if (events.size() > 0) checkEvents(context);
        }
    }

    /**
     * Invoked when there are event. We connect to Google API to query for the last known location
     * and start checking events.
     *
     * @param context context to use.
     */
    private void checkEvents(Context context) {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    /**
     * Returns the range end date -- one hour from now.
     *
     * @param context context to get shared preferences for to store the date as the start date for the next check.
     *
     * @return the end date.
     */
    private Calendar getEndDate(Context context) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, 1);

        SharedPreferences prefs = context.getSharedPreferences(TAG, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("startTime", c.getTimeInMillis());
        editor.apply();

        return c;
    }

    /**
     * Returns the range start date. It's either present moment, or the last range date.
     *
     * @param context context to get shared preferences from.
     *
     * @return the start date.
     */
    private Calendar getStartDate(Context context) {
        Calendar c = Calendar.getInstance();

        SharedPreferences prefs = context.getSharedPreferences(TAG, 0);
        long millis = prefs.getLong("startTime", -1);
        if (millis != -1) c.setTimeInMillis(millis);

        return c;
    }

    /**
     * Called when we finish connecting to Google API client for GPS.
     *
     * @param bundle some args that we don't use.
     */
    public void onConnected(@Nullable Bundle bundle) {
        // If we don't have location permission, report and exit.
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "No ACCESS_COARSE_LOCATION permission granted");
            return;
        }

        // Get current user location (up to block precision).
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation == null) {
            Log.e(TAG, "Unable to detect last known location");
            return;
        }

        // Disconnect. We don't need Google API client any more.
        mGoogleApiClient.disconnect();

        // Run through the found events and check each of them.
        Geocoder coder = new Geocoder(context);
        Location bottomLeft = latLonDestPoint(mLastLocation, 180 + 45, 50);
        Location topRight = latLonDestPoint(mLastLocation, 45, 50);
        Log.d(TAG, "Checking events within " + bottomLeft + " - " + topRight);
        for (Event e : events) checkEvent(coder, mLastLocation, e, bottomLeft, topRight);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Connection failed: " + connectionResult);
    }

    /**
     * See if an event is outside the range threshold from the current user location and
     * fire the notification if it is.
     *
     * @param coder             geo coder for address-to-coords conversion.
     * @param currentLocation   current user location.
     * @param event             event to check.
     * @param bottomLeft        bottom left of the area.
     * @param topRight          top right of the area.
     */
    private void checkEvent(Geocoder coder, Location currentLocation, Event event, Location bottomLeft, Location topRight) {
        Log.d(TAG, "Event: ID=" + event.id + ", Calendar=" + event.calendar + ", Title=" + event.title + ", Location=" + event.location);

        try {
            List<Address> addresses = coder.getFromLocationName(event.location, 1, bottomLeft.getLatitude(), bottomLeft.getLongitude(), topRight.getLatitude(), topRight.getLongitude());
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                Location loc = new Location("");
                loc.setLatitude(address.getLatitude());
                loc.setLongitude(address.getLongitude());

                float distanceMeters = loc.distanceTo(currentLocation);
                Log.d(TAG, "Distance from " + address.toString() + " " + distanceMeters);
                if (distanceMeters > THRESHOLD_METERS) notifyEvent(event, address);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send user notification.
     *
     * @param event   event.
     * @param address event address.
     */
    private void notifyEvent(Event event, Address address) {
        Log.i(TAG, "Notifying about the event: " + event.title + " at " + address.toString() + " start=" + event.startTime + " - end=" + event.endTime);

        Bundle extras = new Bundle();
        extras.putString("eventTitle", event.title);
        extras.putString("eventLocation", event.location);
        extras.putDouble("latitude", address.getLatitude());
        extras.putDouble("longitude", address.getLongitude());
        extras.putLong("eventStartTime", event.startTime);
        extras.putLong("eventEndTime", event.endTime);

        Intent clickIntent = new Intent(context, CheckerNotificationClickActivity.class)
                .putExtras(extras)
                .setFlags(android.content.Intent.FLAG_ACTIVITY_NO_HISTORY);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.app_icon)
                .setAutoCancel(true)
                .setContentTitle("UbiPark")
                .setContentText("Upcoming event at " + event.location)
                .setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.honkhonk))
                .setContentIntent(PendingIntent.getActivity(context, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT));

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    private double radians(double degrees) {
        return degrees * Math.PI / 180.0;
    }

    private double degrees(double radians) {
        return radians * 180.0 / Math.PI;
    }

    private Location latLonDestPoint(Location origin, double bearing, int distance) {
        double brng = radians(bearing);
        double lat1 = radians(origin.getLatitude());
        double lon1 = radians(origin.getLongitude());

        double lat2 = Math.asin(Math.sin(lat1) * Math.cos(distance / EARTH_RADIUS) +
                                Math.cos(lat1) * Math.sin(distance / EARTH_RADIUS) * Math.cos(brng));
        double lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(distance / EARTH_RADIUS) * Math.cos(lat1),
                                                         Math.cos(distance / EARTH_RADIUS) - Math.sin(lat1) * Math.sin(lat2));
        lon2 = ((lon2 + Math.PI) % (2.0 * Math.PI)) - Math.PI;

        Location loc = new Location(origin);
        loc.setLatitude(degrees(lat2));
        loc.setLongitude(degrees(lon2));
        return loc;
    }
}
